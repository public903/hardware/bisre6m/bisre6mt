# BISRE6MT

**BISRE6M** - BIStable RElay 6-Module Top

Top PCB for 6-channel relay module on DIN rail.

> **Specifications:**  
> Contacts: 230VAC / 16A  
> Supply: 10-35VAC or 12-48VDC  
> Inputs: 7-35VAC or 7-48VDC  

